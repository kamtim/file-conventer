import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/StartPage.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/docx',
      name: 'about',
      component: () => import('./views/Docx.vue')
    },
    {
      path: '/pptx',
      name: 'about',
      component: () => import('./views/Pptx.vue')
    },
  ]
})
