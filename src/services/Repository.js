import axios from "axios"

const baseDomain = "https://api.cloudmersive.com";
const baseUrl = `${baseDomain}/convert`;

export default axios.create({
    baseURL: baseUrl,
    headers: {Apikey: "d89afe95-c616-4e8c-bae3-1bcb80a18175"},
    responseType: "arraybuffer"
})