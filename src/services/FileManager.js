export default  {
    download(response, file) {
        var nameWithoutExtension = file.name.slice(0, file.name.lastIndexOf('.'));
        var headers = response.headers;
        var blob = new Blob([response.data],{type:headers['content-type']});
        const url = window.URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = url;
        link.setAttribute('download', nameWithoutExtension +'.pdf'); //or any other extension
        document.body.appendChild(link);
        link.click();
    }
}