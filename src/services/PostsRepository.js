import Repository from "./Repository";

const urlDocxToPdf = "/docx/to/pdf";
const urlPptxToPdf = "/pptx/to/pdf";

export default {
    docxToPdf(file) {
        return Repository.post(`${urlDocxToPdf}`, file);
    },

    pptxToPdf(file) {
        return Repository.post(`${urlPptxToPdf}`, file);
    }


}